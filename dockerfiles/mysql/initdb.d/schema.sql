-- MySQL dump 10.17  Distrib 10.3.12-MariaDB, for Win64 (AMD64)
--
-- Host: 192.168.1.131    Database: MCJAPPDB
-- ------------------------------------------------------
-- Server version	5.5.60-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- MCJAPPDB のデータベース構造をダンプしています
CREATE DATABASE IF NOT EXISTS `MCJAPPDB` /*!40100 DEFAULT CHARACTER SET utf8 */;
GRANT ALL PRIVILEGES ON MCJAPPDB.* TO MCJAPP@"%" IDENTIFIED BY 'MCJAPP' WITH GRANT  OPTION;

USE `MCJAPPDB`;

--
-- Table structure for table `m_app_config`
--
DROP TABLE IF EXISTS `m_app_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_app_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `app_type` int(11) NOT NULL COMMENT '対象アプリ:0.HMアプリ / 1.MCJアプリ / 2.両方',
  `data_type` int(11) DEFAULT NULL COMMENT 'データ種別:データ種別：0.URL, 1.サーバpath, 2.強制アップデート対応バージョン, 3.証明書アップロード種別',
  `name` varchar(256) DEFAULT NULL COMMENT 'マスタ名',
  `param` varchar(256) DEFAULT NULL COMMENT 'マスタ値',
  `detail` varchar(256) DEFAULT NULL COMMENT 'マスタ値説明',
  `created` datetime NOT NULL COMMENT '作成日',
  `modified` datetime NOT NULL COMMENT '更新日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9012 DEFAULT CHARSET=utf8 COMMENT='アプリマスタ設定';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_area`
--

DROP TABLE IF EXISTS `m_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_area` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `area_cd` varchar(2) NOT NULL COMMENT '地域コード:前ゼロ編集、都道府県コード',
  `name` varchar(80) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='地域マスタ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_banner_manage`
--

DROP TABLE IF EXISTS `m_banner_manage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_banner_manage` (
  `id` int(11) NOT NULL COMMENT 'id',
  `disp_space` int(11) NOT NULL COMMENT '表示箇所:0.HMアプリTOP上部バナー',
  `file_name` varchar(256) NOT NULL COMMENT 'ファイル名',
  `url` varchar(256) DEFAULT NULL COMMENT 'リンク先',
  `detail` varchar(256) DEFAULT NULL COMMENT '詳細説明',
  `disp_order` int(11) NOT NULL COMMENT '表示順',
  `disp_start` datetime NOT NULL COMMENT '表示期限(開始)',
  `disp_end` datetime DEFAULT NULL COMMENT '表示期限(終了)',
  `created` datetime NOT NULL COMMENT '作成日',
  `modified` datetime NOT NULL COMMENT '更新日',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='バナー管理マスタ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_banner_manager`
--

DROP TABLE IF EXISTS `m_banner_manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_banner_manager` (
  `id` int(11) NOT NULL COMMENT 'id',
  `disp_space` int(11) NOT NULL COMMENT '表示箇所:0.HMアプリTOP上部バナー',
  `file_name` varchar(256) NOT NULL COMMENT 'ファイル名',
  `url` varchar(256) DEFAULT NULL COMMENT 'リンク先',
  `detail` varchar(256) DEFAULT NULL COMMENT '詳細説明',
  `disp_order` int(11) NOT NULL COMMENT '表示順',
  `disp_start` datetime NOT NULL COMMENT '表示期限(開始)',
  `disp_end` datetime DEFAULT NULL COMMENT '表示期限(終了)',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='バナー管理テーブル';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_era`
--

DROP TABLE IF EXISTS `m_era`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_era` (
  `EraCd` char(2) NOT NULL COMMENT '元号コード',
  `EraNam` varchar(80) DEFAULT NULL COMMENT '元号名称',
  `FrmYmd` char(8) DEFAULT NULL COMMENT '開始年月日',
  `ToYmd` char(8) DEFAULT NULL COMMENT '終了年月日',
  `UpdTms` int(11) NOT NULL COMMENT '修正回数',
  `LgcDelFlg` char(1) NOT NULL COMMENT '論理削除フラグ',
  `DatUpdYmd` date NOT NULL COMMENT 'データ修正年月日',
  `DatUpdCmpCod` varchar(2) NOT NULL COMMENT 'データ修正会社コード',
  `DatUpdLgnId` varchar(8) NOT NULL COMMENT 'データ修正ログインID',
  `DatRegYmd` date NOT NULL COMMENT 'データ登録年月日',
  `DatRegCmpCod` varchar(2) NOT NULL COMMENT 'データ登録会社コード',
  `DatRegLgnId` varchar(8) NOT NULL COMMENT 'データ登録ログインID',
  `EraSgn` char(1) DEFAULT NULL COMMENT '和暦記号',
  `EraKbn` char(1) DEFAULT NULL COMMENT 'カード用元号区分',
  `Frml` int(11) DEFAULT NULL COMMENT '計算式',
  PRIMARY KEY (`EraCd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='元号マスタ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_hm_cmp`
--

DROP TABLE IF EXISTS `m_hm_cmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_hm_cmp` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `cmp_cd` varchar(2) NOT NULL COMMENT '会社コード',
  `name` varchar(80) DEFAULT NULL COMMENT '会社名',
  `delete_flg` char(1) DEFAULT NULL COMMENT '削除フラグ',
  `create_user_id` varchar(12) DEFAULT NULL COMMENT '作成ユーザID',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modify_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COMMENT='HMマスタ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_hm_salesoffice`
--

DROP TABLE IF EXISTS `m_hm_salesoffice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_hm_salesoffice` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `cmp_cd` varchar(2) NOT NULL COMMENT '会社コード',
  `slo_cd` varchar(8) NOT NULL COMMENT '営業所コード',
  `name` varchar(80) DEFAULT NULL COMMENT '営業所名',
  `area_cd` varchar(2) NOT NULL COMMENT '地域コード',
  `premium_flg` char(1) DEFAULT NULL COMMENT 'プレミアムフラグ',
  `premium_main_user_id` varchar(12) DEFAULT NULL COMMENT 'プレミアムMCJ担当(メイン)',
  `premium_sub_user_id` varchar(12) DEFAULT NULL COMMENT 'プレミアムMCJ担当(サブ)',
  `delete_flg` char(1) DEFAULT NULL COMMENT '削除フラグ',
  `create_user_id` varchar(12) DEFAULT NULL COMMENT '作成ユーザID',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modify_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COMMENT='HM営業所マスタ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_hm_salesoffice_loanplazagroup`
--

DROP TABLE IF EXISTS `m_hm_salesoffice_loanplazagroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_hm_salesoffice_loanplazagroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `hm_cmp_cd` varchar(2) NOT NULL COMMENT 'HMコード',
  `slo_cd` varchar(8) NOT NULL COMMENT '営業所コード',
  `loanplazagroup_cd` varchar(12) NOT NULL COMMENT 'ローンプラザグループCD',
  `create_user_id` varchar(12) DEFAULT NULL COMMENT '作成ユーザID',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modify_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COMMENT='HM営業所-ローンプラザグループ紐づけマスタ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_hm_user`
--

DROP TABLE IF EXISTS `m_hm_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_hm_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `cmp_cd` varchar(2) NOT NULL COMMENT '会社コード',
  `slo_cd` varchar(8) NOT NULL COMMENT '営業所コード',
  `user_id` varchar(12) NOT NULL COMMENT 'ユーザID',
  `name` varchar(80) DEFAULT NULL COMMENT 'ユーザ名',
  `kana_name` varchar(80) DEFAULT NULL COMMENT 'ユーザ名カナ',
  `premium_flg` char(1) DEFAULT NULL COMMENT 'プレミアムフラグ',
  `premium_main_user_id` varchar(12) DEFAULT NULL COMMENT 'プレミアムMCJ担当(メイン)',
  `premium_sub_user_id` varchar(12) DEFAULT NULL COMMENT 'プレミアムMCJ担当(サブ)',
  `delete_flg` char(1) DEFAULT NULL COMMENT '削除フラグ',
  `create_user_id` varchar(12) DEFAULT NULL COMMENT '作成ユーザID',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modify_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COMMENT='HMユーザマスタ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_hm_user_chat`
--

DROP TABLE IF EXISTS `m_hm_user_chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_hm_user_chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `cmp_cd` varchar(2) NOT NULL COMMENT '会社コード',
  `user_id` varchar(12) NOT NULL COMMENT 'ユーザID',
  `chat_username` varchar(256) DEFAULT NULL COMMENT 'チャットユーザ名',
  `chat_password` varchar(256) DEFAULT NULL COMMENT 'チャットパスワード',
  `chat_room_id` varchar(256) DEFAULT NULL COMMENT 'チャットルームID',
  `create_user_id` varchar(12) DEFAULT NULL COMMENT '作成ユーザID',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modify_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COMMENT='HMユーザチャット情報マスタ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_loanplazagroup`
--

DROP TABLE IF EXISTS `m_loanplazagroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_loanplazagroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `code` varchar(12) NOT NULL COMMENT 'グループコード',
  `name` varchar(80) DEFAULT NULL COMMENT 'グループ名称',
  `tel` varchar(15) DEFAULT NULL COMMENT 'グループ電話番号',
  `delete_flg` char(1) DEFAULT NULL COMMENT '削除フラグ',
  `create_user_id` varchar(12) DEFAULT NULL COMMENT '作成ユーザID',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modify_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='ローンプラザグループマスタ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_mbbs_code`
--

DROP TABLE IF EXISTS `m_mbbs_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_mbbs_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `code_key` int(11) NOT NULL COMMENT '項番',
  `code_logical_name` varchar(256) DEFAULT NULL COMMENT '項目名',
  `code_name` varchar(256) DEFAULT NULL COMMENT '項目物理名',
  `code` varchar(32) NOT NULL COMMENT 'キー',
  `value` varchar(256) DEFAULT NULL COMMENT '値',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=594 DEFAULT CHARSET=utf8 COMMENT='MBBSコードマスタ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_mcj_holiday`
--

DROP TABLE IF EXISTS `m_mcj_holiday`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_mcj_holiday` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `ymd` date NOT NULL COMMENT '年月日',
  `disp_name` varchar(80) DEFAULT NULL COMMENT '表示名',
  `delete_flg` char(1) NOT NULL COMMENT '削除フラグ',
  `create_user_id` varchar(12) DEFAULT NULL COMMENT '作成ユーザID',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modify_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='MCJ休日マスタ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_mcj_user`
--

DROP TABLE IF EXISTS `m_mcj_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_mcj_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` varchar(12) NOT NULL COMMENT 'ユーザID',
  `name` varchar(80) DEFAULT NULL COMMENT 'ユーザ名',
  `kana_name` varchar(80) DEFAULT NULL COMMENT 'ユーザ名カナ',
  `tel` varchar(15) DEFAULT NULL COMMENT '電話番号',
  `special_tel` varchar(15) DEFAULT NULL COMMENT '時間外電話番号',
  `image_modified` datetime DEFAULT NULL COMMENT '顔画像更新日時',
  `delete_flg` char(1) DEFAULT NULL COMMENT '削除フラグ',
  `create_user_id` varchar(12) DEFAULT NULL COMMENT '作成ユーザID',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modify_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=169 DEFAULT CHARSET=utf8 COMMENT='MCJユーザマスタ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_mcj_user_chat`
--

DROP TABLE IF EXISTS `m_mcj_user_chat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_mcj_user_chat` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` varchar(12) NOT NULL COMMENT 'ユーザID',
  `chat_username` varchar(256) DEFAULT NULL COMMENT 'チャットユーザ名',
  `chat_password` varchar(256) DEFAULT NULL COMMENT 'チャットパスワード',
  `create_user_id` varchar(12) DEFAULT NULL COMMENT '作成ユーザID',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modify_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8 COMMENT='MCJユーザチャット情報マスタ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_mcj_user_image`
--

DROP TABLE IF EXISTS `m_mcj_user_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_mcj_user_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` varchar(12) NOT NULL COMMENT 'ユーザID',
  `image` mediumtext COMMENT '画像',
  `create_user_id` varchar(12) DEFAULT NULL COMMENT '作成ユーザID',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modify_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='MCJユーザ顔画像マスタ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_mcj_user_m_loanplazagroup`
--

DROP TABLE IF EXISTS `m_mcj_user_m_loanplazagroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_mcj_user_m_loanplazagroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` varchar(12) NOT NULL COMMENT 'ユーザID',
  `loanplazagroup_cd` varchar(12) NOT NULL COMMENT 'ローンプラザグループコード',
  `sort_idx` int(11) DEFAULT NULL COMMENT 'ソートインデックス',
  `hide_flg` char(1) DEFAULT NULL COMMENT '非表示フラグ:0:表示、1:非表示',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8 COMMENT='ユーザ-ローンプラザグループ紐づけマスタ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_mcj_user_m_role`
--

DROP TABLE IF EXISTS `m_mcj_user_m_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_mcj_user_m_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` varchar(12) NOT NULL COMMENT 'ユーザID',
  `role_id` int(11) NOT NULL COMMENT 'ロールID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8 COMMENT='ユーザ-ロール紐づけマスタ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_message`
--

DROP TABLE IF EXISTS `m_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `send_func` int(11) NOT NULL COMMENT '送信元処理:0.登録完了時 / 1.プレ審査登録完了時 …',
  `send_target` int(11) DEFAULT NULL COMMENT '送信対象:0.HMユーザー / 1.MCJアプリユーザー / 2.すべて',
  `revision` int(11) NOT NULL DEFAULT '1' COMMENT 'メッセージ文言変更履歴',
  `message` varchar(256) NOT NULL COMMENT 'メッセージ文言',
  `send_type` int(11) NOT NULL COMMENT '送信種別:0.Push通知 / 1.SMS',
  `title` varchar(256) DEFAULT NULL COMMENT 'タイトル',
  `push_type` int(11) DEFAULT NULL COMMENT 'プッシュ通知種別:アプリ用連携コード',
  PRIMARY KEY (`id`),
  KEY `m_message_IX1` (`send_func`,`send_type`),
  KEY `m_message_IX2` (`id`,`send_func`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='メッセージ文言マスタ:';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_role`
--

DROP TABLE IF EXISTS `m_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `code` int(11) NOT NULL COMMENT 'ロール コード',
  `name` varchar(256) DEFAULT NULL COMMENT 'ロール名称',
  `delete_flg` char(1) DEFAULT NULL COMMENT '削除フラグ',
  `create_user_id` varchar(12) DEFAULT NULL COMMENT '作成ユーザID',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modify_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='ロールマスタ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `m_srp_hm_user_mcj_user`
--

DROP TABLE IF EXISTS `m_srp_hm_user_mcj_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `m_srp_hm_user_mcj_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `srp_id` varchar(10) DEFAULT NULL COMMENT '申請ID',
  `hm_cmp_cd` varchar(2) DEFAULT NULL COMMENT 'HMコード',
  `hm_user_id` varchar(12) DEFAULT NULL COMMENT 'HMユーザID',
  `mcj_user_id` varchar(12) DEFAULT NULL COMMENT 'MCJユーザID',
  `create_user_id` varchar(12) DEFAULT NULL COMMENT '作成ユーザID',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modify_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=804 DEFAULT CHARSET=utf8 COMMENT='案件担当紐づけマスタ';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_area_info`
--

DROP TABLE IF EXISTS `t_area_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_area_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `title` varchar(256) NOT NULL COMMENT 'タイトル',
  `article` text COMMENT '記事',
  `disp_start` datetime NOT NULL COMMENT '表示期限(開始)',
  `disp_end` datetime DEFAULT NULL COMMENT '表示期限(終了)',
  `publish_flg` char(1) NOT NULL COMMENT '公開フラグ:0: 非公開、1:公開',
  `delete_flg` char(1) NOT NULL COMMENT '削除フラグ:0:未削除、1:削除',
  `create_user_id` varchar(12) DEFAULT NULL COMMENT '作成ユーザID',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modify_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8 COMMENT='地域情報テーブル';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_area_info_area`
--

DROP TABLE IF EXISTS `t_area_info_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_area_info_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `area_info_id` int(11) NOT NULL COMMENT '地域情報ID',
  `area_cd` varchar(2) NOT NULL COMMENT '地域コード',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5014 DEFAULT CHARSET=utf8 COMMENT='地域情報-地域紐づけテーブル';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_area_info_hm`
--

DROP TABLE IF EXISTS `t_area_info_hm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_area_info_hm` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `area_info_id` int(11) NOT NULL COMMENT '地域情報ID',
  `cmp_cd` varchar(2) NOT NULL COMMENT 'HM会社コード',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4393 DEFAULT CHARSET=utf8 COMMENT='地域情報-HM紐づけテーブル';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_banner`
--

DROP TABLE IF EXISTS `t_banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `disp_space` int(11) NOT NULL COMMENT '表示箇所:0.HMアプリTOP上部バナー',
  `file_name` varchar(256) NOT NULL COMMENT 'ファイル名',
  `image_data` mediumtext COMMENT '画像データ',
  `url` varchar(256) DEFAULT NULL COMMENT 'リンク先',
  `detail` varchar(256) DEFAULT NULL COMMENT '詳細説明',
  `disp_start` datetime NOT NULL COMMENT '表示期限(開始)',
  `disp_end` datetime DEFAULT NULL COMMENT '表示期限(終了)',
  `publish_flg` char(1) NOT NULL COMMENT '公開フラグ:0: 非公開、1:公開',
  `delete_flg` char(1) NOT NULL COMMENT '削除フラグ:0:未削除、1:削除',
  `create_user_id` varchar(12) DEFAULT NULL COMMENT '作成ユーザID',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modify_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='キャンペーンバナーテーブル';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_banner_sort`
--

DROP TABLE IF EXISTS `t_banner_sort`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_banner_sort` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `banner_id` int(11) NOT NULL COMMENT 'バナーid',
  `sort_rank` int(11) NOT NULL COMMENT 'ソート順',
  `create_user_id` varchar(12) DEFAULT NULL COMMENT '作成ユーザID',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modify_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='キャンペーンバナー並び順テーブル';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_batch_log`
--

DROP TABLE IF EXISTS `t_batch_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_batch_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `batch_no` int(11) NOT NULL COMMENT 'バッチ番号',
  `batch_seq` int(11) NOT NULL COMMENT 'バッチ内連番',
  `batch_date` date DEFAULT NULL COMMENT 'バッチ日付',
  `batch_status` int(11) DEFAULT NULL COMMENT 'バッチ実行状況:0：未実行, 1：実行中, 2：終了, 9：異常終了',
  `start_time` datetime DEFAULT NULL COMMENT '開始日時',
  `end_time` datetime DEFAULT NULL COMMENT '終了日時',
  `create_user_id` varchar(12) DEFAULT NULL COMMENT '作成ユーザID',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modify_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8 COMMENT='バッチ履歴テーブル';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_cert_history`
--

DROP TABLE IF EXISTS `t_cert_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_cert_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `srp_id` varchar(10) NOT NULL COMMENT '申込番号',
  `approval_type` int(11) NOT NULL COMMENT '申込承認種別',
  `user_id` varchar(12) NOT NULL COMMENT 'ユーザID',
  `status` varchar(1) DEFAULT '0' COMMENT 'ステータス:0:-, 1:○, 9:×',
  `apply_info` text COMMENT '申込内容',
  `customer_info` text COMMENT '顧客情報',
  `srp_upd_tms` int(11) DEFAULT NULL COMMENT '申込更新回数',
  `create_user_id` varchar(12) DEFAULT NULL COMMENT '作成ユーザID',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modify_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=183 DEFAULT CHARSET=utf8 COMMENT='認証履歴テーブル';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_cert_work`
--

DROP TABLE IF EXISTS `t_cert_work`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_cert_work` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `srp_id` varchar(10) NOT NULL COMMENT '申込番号',
  `approval_type` int(11) NOT NULL COMMENT '申込承認種別',
  `user_id` varchar(12) NOT NULL COMMENT 'ユーザID',
  `apply_info` text COMMENT '申込内容',
  `customer_info` text COMMENT '顧客情報',
  `srp_upd_tms` int(11) DEFAULT NULL COMMENT '申込更新回数',
  `create_user_id` varchar(12) DEFAULT NULL COMMENT '作成ユーザID',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modify_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=752 DEFAULT CHARSET=utf8 COMMENT='認証ワークテーブル';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_device_token`
--

DROP TABLE IF EXISTS `t_device_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_device_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `app_type` int(11) NOT NULL COMMENT 'アプリ種別:0.HMアプリ / 1.MCJアプリ',
  `user_id` varchar(12) DEFAULT NULL COMMENT 'ユーザID',
  `cmp_code` varchar(32) DEFAULT NULL COMMENT 'HMユーザの会社コード',
  `arn` varchar(256) DEFAULT NULL COMMENT 'ARN',
  `push_token` varchar(512) DEFAULT NULL COMMENT 'プッシュトークン',
  `os_type` int(11) NOT NULL COMMENT 'OS区分:0.iOS /1.Android',
  `device_token` varchar(36) NOT NULL COMMENT 'デバイストークン',
  `delete_flg` char(1) NOT NULL COMMENT '削除フラグ:0:有効データ, 1:削除データ',
  `created` datetime NOT NULL COMMENT '作成日',
  `modified` datetime NOT NULL COMMENT '更新日',
  PRIMARY KEY (`id`),
  KEY `t_device_token_IX1` (`user_id`,`cmp_code`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COMMENT='デバイストークン';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_important_document`
--

DROP TABLE IF EXISTS `t_important_document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_important_document` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `doc_type` int(11) NOT NULL COMMENT '文書種別:0.利用規約 / 1.電磁的同意書 / 2.個人情報の取扱いに関する同意書 / 3.重要事項説明書',
  `doc_manage_no` varchar(256) DEFAULT NULL COMMENT '文書管理番号',
  `version` int(11) NOT NULL COMMENT 'バージョン',
  `disp_start` datetime NOT NULL COMMENT '表示開始日',
  `disp_end` datetime DEFAULT NULL COMMENT '表示終了日',
  `hide_flg` char(1) DEFAULT NULL COMMENT '非表示フラグ:0.表示、1.非表示',
  `detail` mediumtext COMMENT '内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COMMENT='重要文書テーブル';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_infomation`
--

DROP TABLE IF EXISTS `t_infomation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_infomation` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `title` varchar(256) NOT NULL COMMENT 'タイトル',
  `article` text COMMENT '記事',
  `disp_start` datetime NOT NULL COMMENT '表示期限(開始)',
  `disp_end` datetime DEFAULT NULL COMMENT '表示期限(終了)',
  `publish_flg` char(1) NOT NULL COMMENT '公開フラグ:0: 非公開、1:公開',
  `delete_flg` char(1) NOT NULL COMMENT '削除フラグ:0:未削除、1:削除',
  `create_user_id` varchar(12) DEFAULT NULL COMMENT '作成ユーザID',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modify_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='お知らせ情報テーブル';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_infomation_hm`
--

DROP TABLE IF EXISTS `t_infomation_hm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_infomation_hm` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `infomation_id` int(11) NOT NULL COMMENT 'お知らせID',
  `cmp_cd` varchar(2) NOT NULL COMMENT 'HM会社コード',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='お知らせ-HM紐づけテーブル';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_maintenance`
--

DROP TABLE IF EXISTS `t_maintenance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_maintenance` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `start_date` datetime NOT NULL COMMENT '開始日時',
  `end_date` datetime DEFAULT NULL COMMENT '終了日時',
  `mainte_type` int(11) NOT NULL COMMENT 'メンテナンス種別:0.AWSメンテ / 1.MBBSメンテ',
  `mainte_status` int(11) DEFAULT '1' COMMENT 'メンテナンス状態:0.稼働中 / 1.メンテナンス中',
  `message` varchar(256) DEFAULT NULL COMMENT '表示文言:固定文言以外を表示したい場合に使用',
  `create_user_id` varchar(12) DEFAULT NULL COMMENT '作成ユーザID',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modify_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`),
  KEY `t_maintenance_IX1` (`start_date`,`end_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='アプリメンテナンス管理';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_maintenance_regularly`
--

DROP TABLE IF EXISTS `t_maintenance_regularly`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_maintenance_regularly` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `regularly_type` int(11) NOT NULL COMMENT '定期日区分',
  `start_time` time NOT NULL COMMENT '開始時間',
  `end_time` time NOT NULL COMMENT '終了時間',
  `mainte_type` int(11) NOT NULL COMMENT 'メンテナンス種別:0.AWSメンテ / 1.MBBSメンテ',
  `mainte_status` int(11) DEFAULT '1' COMMENT 'メンテナンス状態:0.稼働中 / 1.メンテナンス中',
  `message` varchar(256) DEFAULT NULL COMMENT '表示文言:固定文言以外を表示したい場合に使用',
  `create_user_id` varchar(12) DEFAULT NULL COMMENT '作成ユーザID',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modify_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`),
  KEY `t_maintenance_regularly_IX1` (`regularly_type`,`end_time`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='アプリ定期メンテナンス管理';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_news_body`
--

DROP TABLE IF EXISTS `t_news_body`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_news_body` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `news_header_id` int(11) NOT NULL COMMENT 'ニュースヘッダーID',
  `disp_sort` int(11) NOT NULL COMMENT '表示順',
  `article_type` int(11) NOT NULL COMMENT '記事タイプ',
  `title` text COMMENT '記事タイトル',
  `article` mediumtext COMMENT '記事内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=233 DEFAULT CHARSET=utf8 COMMENT='経済ニュースデータテーブル';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_news_header`
--

DROP TABLE IF EXISTS `t_news_header`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_news_header` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `title` varchar(256) NOT NULL COMMENT 'タイトル',
  `news_no` int(11) DEFAULT NULL COMMENT 'ニュース番号',
  `disp_start` datetime NOT NULL COMMENT '表示期限(開始)',
  `disp_end` datetime DEFAULT NULL COMMENT '表示期限(終了)',
  `publish_flg` char(1) NOT NULL COMMENT '公開フラグ:0: 非公開、1:公開',
  `delete_flg` char(1) NOT NULL COMMENT '削除フラグ:0:未削除、1:削除',
  `create_user_id` varchar(12) DEFAULT NULL COMMENT '作成ユーザID',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modify_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='経済ニュースヘッダーテーブル';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_push_sent`
--

DROP TABLE IF EXISTS `t_push_sent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_push_sent` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `send_target` int(11) NOT NULL COMMENT '送信対象:0.HMユーザー / 1.MCJアプリユーザー / 2.すべて',
  `sent_user_id` varchar(12) DEFAULT NULL COMMENT '送信者ユーザーID',
  `received_user_id` varchar(12) DEFAULT NULL COMMENT '受信者者ユーザーID',
  `cmp_code` varchar(32) DEFAULT NULL COMMENT 'HMユーザの会社コード',
  `message_id` int(11) DEFAULT NULL COMMENT 'メッセージID',
  `message` varchar(256) DEFAULT NULL COMMENT '送付メッセージ',
  `sent_date` datetime DEFAULT NULL COMMENT '送付日時',
  PRIMARY KEY (`id`),
  KEY `t_push_sent_IX1` (`cmp_code`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COMMENT='プッシュ通知送付履歴';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_sitting`
--

DROP TABLE IF EXISTS `t_sitting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_sitting` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` varchar(12) NOT NULL COMMENT 'ユーザID',
  `sitting_flg` char(1) NOT NULL COMMENT '在席フラグ',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8 COMMENT='在席テーブル';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_sms_sent`
--

DROP TABLE IF EXISTS `t_sms_sent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_sms_sent` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `send_target` int(11) NOT NULL COMMENT '送信対象:0.HMユーザー / 1.MCJアプリユーザー / 2.すべて',
  `sent_user_id` varchar(12) DEFAULT NULL COMMENT '送信者ユーザーID',
  `received_user_id` varchar(12) DEFAULT NULL COMMENT '受信者者ユーザーID',
  `cmp_code` varchar(32) DEFAULT NULL COMMENT 'HMユーザの会社コード',
  `message_id` int(11) DEFAULT NULL COMMENT 'メッセージID',
  `message` varchar(256) DEFAULT NULL COMMENT '送付メッセージ',
  `sent_date` datetime DEFAULT NULL COMMENT '送付日時',
  PRIMARY KEY (`id`),
  KEY `t_sms_sent_IX1` (`cmp_code`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8 COMMENT='SMSメッセージ送付履歴';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_srp_user_relate`
--

DROP TABLE IF EXISTS `t_srp_user_relate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_srp_user_relate` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `srp_id` varchar(10) NOT NULL COMMENT '申請ID',
  `user_id` varchar(12) NOT NULL COMMENT 'ユーザID',
  `act_div` int(11) NOT NULL COMMENT '役割区分',
  `delete_flg` char(1) NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8 COMMENT='ユーザ－申込紐づけ情報';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `t_user_confirm_view_state`
--

DROP TABLE IF EXISTS `t_user_confirm_view_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_user_confirm_view_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `srp_id` varchar(10) NOT NULL COMMENT '申込番号',
  `user_id` varchar(12) NOT NULL COMMENT 'ユーザID',
  `read_flg` char(1) DEFAULT '0' COMMENT '閲覧済フラグ:0:未済, 1:済',
  `read_date` datetime DEFAULT NULL COMMENT '閲覧日時',
  `create_user_id` varchar(12) DEFAULT NULL COMMENT '作成ユーザID',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modify_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COMMENT='内容確認閲覧状況テーブル';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `w_hm_cmp`
--

DROP TABLE IF EXISTS `w_hm_cmp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w_hm_cmp` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `cmp_cd` varchar(2) NOT NULL COMMENT '会社コード',
  `name` varchar(80) DEFAULT NULL COMMENT '会社名',
  `delete_flg` char(1) DEFAULT NULL COMMENT '削除フラグ',
  `create_user_id` varchar(12) DEFAULT NULL COMMENT '作成ユーザID',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modify_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 COMMENT='HMワーク';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `w_hm_salesoffice`
--

DROP TABLE IF EXISTS `w_hm_salesoffice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w_hm_salesoffice` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `cmp_cd` varchar(2) NOT NULL COMMENT '会社コード',
  `slo_cd` varchar(8) NOT NULL COMMENT '営業所コード',
  `name` varchar(80) DEFAULT NULL COMMENT '営業所名',
  `area_cd` varchar(2) NOT NULL COMMENT '地域コード',
  `premium_flg` char(1) DEFAULT NULL COMMENT 'プレミアムフラグ',
  `premium_main_user_id` varchar(12) DEFAULT NULL COMMENT 'プレミアムMCJ担当(メイン)',
  `premium_sub_user_id` varchar(12) DEFAULT NULL COMMENT 'プレミアムMCJ担当(サブ)',
  `delete_flg` char(1) DEFAULT NULL COMMENT '削除フラグ',
  `create_user_id` varchar(12) DEFAULT NULL COMMENT '作成ユーザID',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modify_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COMMENT='HM営業所ワーク';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `w_hm_user`
--

DROP TABLE IF EXISTS `w_hm_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w_hm_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `cmp_cd` varchar(2) NOT NULL COMMENT '会社コード',
  `slo_cd` varchar(8) NOT NULL COMMENT '営業所コード',
  `user_id` varchar(12) NOT NULL COMMENT 'ユーザID',
  `name` varchar(80) DEFAULT NULL COMMENT 'ユーザ名',
  `kana_name` varchar(80) DEFAULT NULL COMMENT 'ユーザ名カナ',
  `premium_flg` char(1) DEFAULT NULL COMMENT 'プレミアムフラグ',
  `premium_main_user_id` varchar(12) DEFAULT NULL COMMENT 'プレミアムMCJ担当(メイン)',
  `premium_sub_user_id` varchar(12) DEFAULT NULL COMMENT 'プレミアムMCJ担当(サブ)',
  `delete_flg` char(1) DEFAULT NULL COMMENT '削除フラグ',
  `create_user_id` varchar(12) DEFAULT NULL COMMENT '作成ユーザID',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modify_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COMMENT='HMユーザワーク';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `w_mcj_user`
--

DROP TABLE IF EXISTS `w_mcj_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w_mcj_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` varchar(12) NOT NULL COMMENT 'ユーザID',
  `name` varchar(80) DEFAULT NULL COMMENT 'ユーザ名',
  `kana_name` varchar(80) DEFAULT NULL COMMENT 'ユーザ名カナ',
  `tel` varchar(15) DEFAULT NULL COMMENT '電話番号',
  `special_tel` varchar(15) DEFAULT NULL COMMENT '時間外電話番号',
  `image_modified` datetime DEFAULT NULL COMMENT '顔画像更新日時',
  `delete_flg` char(1) DEFAULT NULL COMMENT '削除フラグ',
  `create_user_id` varchar(12) DEFAULT NULL COMMENT '作成ユーザID',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modify_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=169 DEFAULT CHARSET=utf8 COMMENT='MCJユーザワーク';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `w_srp_hm_user_mcj_user`
--

DROP TABLE IF EXISTS `w_srp_hm_user_mcj_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `w_srp_hm_user_mcj_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `srp_id` varchar(10) DEFAULT NULL COMMENT '申請ID',
  `hm_cmp_cd` varchar(2) DEFAULT NULL COMMENT 'HMコード',
  `hm_user_id` varchar(12) DEFAULT NULL COMMENT 'HMユーザID',
  `mcj_user_id` varchar(12) DEFAULT NULL COMMENT 'MCJユーザID',
  `create_user_id` varchar(12) DEFAULT NULL COMMENT '作成ユーザID',
  `created` datetime NOT NULL COMMENT '作成日時',
  `modify_user_id` varchar(12) DEFAULT NULL COMMENT '更新ユーザID',
  `modified` datetime NOT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=816 DEFAULT CHARSET=utf8 COMMENT='案件担当紐づけワーク';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-11 10:19:33
